# ESP01 IOT Controller

## Objective
This repository holds plans to use an esp01 module. This envolves the hardware schematics and the necessary code to use two of them together as a client - server application. This is usefull for multiple IOT purposes.

## Requirements

- Arduino board (or some USB FTDI board)
- two esp01 modules
- usb cable
- arduino IDE
- fritzing for schematics modeling

## Blue prints

The blueprints for the hardware are available as fritzing schematics

## Uploading code to ESP01

To upload is basically a chaos (LOL). You will need to make the circuit presented here: https://www.behind-the-scenes.co.za/connecting-the-esp8266-to-a-breadboard-and-ftdi-programmer/


## Software

One module acts as a server and controller of the charge. Its going to be the access point for the other module.

The second module acts as a client, connecting to the server's access point and sending a command thru http. This command is going to be processed by the server and them switch the state of a pin, which will be controlling the charge.
