/*
 *  This sketch sends data via HTTP GET requests to data.sparkfun.com service.
 *
 *  You need to get streamId and privateKey at data.sparkfun.com and paste them
 *  below. Or just customize this script to talk to other HTTP servers.
 *
 */

#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <EEPROM.h>

#define PWD_SIZE 12
#define SSID_SIZE 10

#define MEM_ALOC_SIZE (10+SSID_SIZE+PWD_SIZE+4)

char* ssid     = "jadelabs";
char* password = "jadelabs";
bool APMode = false;

char* host = "google.com";

ESP8266WebServer server(80);

void writeFlash(String content, int offset, int size){
  EEPROM.begin(MEM_ALOC_SIZE);

  unsigned char *p=new unsigned char[content.length()];
  strcpy((char *)p,content.c_str());  
  Serial.printf("\nwriting flash...\n");
  for (int i=0; i< size;i++){
    if (i< content.length()) {
      Serial.printf("writing position %i:%c %i\n",i+offset,p[i],p[i]);
      EEPROM.write(i+offset,p[i]);
    }
    else{
      Serial.printf("writing position %i:%c %i\n",i+offset,0,0);
      EEPROM.write(i+offset,0x00);
    }
  }
  Serial.printf("\nre-reading configuration...\n>");

  for(int i=0;i<content.length();i++){
    Serial.printf("%c",EEPROM.read(i+offset));
  }
  Serial.println("<");
  EEPROM.end();  
}

void clearFlash(){
  EEPROM.begin(MEM_ALOC_SIZE);

  for (int i=0; i< 10+SSID_SIZE+PWD_SIZE+4;i++){
      Serial.printf("writing position %i:%i\n",i,0);
      EEPROM.write(i,0x00);
    }
  EEPROM.end();  
}

//copy flash content as char*, starting from 'position' and reading 'size' bytes
char* readFlash(int position, int size){
  unsigned char retuchar[size+1];
  unsigned char chr;
  
  EEPROM.begin(MEM_ALOC_SIZE);
  Serial.printf("\nreading:>");
  int realsize =size;
  for(int i=0; i < size;i++){
    chr = EEPROM.read(position+i);
    //0-9 a-z A-Z
    if ((chr>=48 and chr<= 57) or (chr>=64 and chr<=90) or (chr>=97 and chr<=122)){
      retuchar[i]= chr;
      Serial.printf("%i ",retuchar[i]);
      realsize= i+1;
    }
    else
      retuchar[i]= 0x00;
  }
  retuchar[size]= 0x00;
  Serial.printf("< realsize = %i\n",realsize);
  char *retchar = new char[realsize+1];
  memcpy(retchar,retuchar, realsize+1);
  Serial.printf("retchar=%s\n",retchar);
  return retchar;
  
}

//returns a String representing an IP address, read from position, 4 bytes.
char* readFlashIP(int position){
  return "google.com";
  
}

void wifiSetup(){
  Serial.print("Configuring access point...\n");
  /* You can remove the password parameter if you want the AP to be open. */
  Serial.printf(">%s<\n",ssid);
  Serial.printf(">%s<\n",password);
  WiFi.disconnect();
  WiFi.mode(WIFI_AP_STA);
  WiFi.softAP(ssid, password);
  //WiFi.softAP("DoorCtrl", "12345678");
  
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.on("/set", handleSet);
  server.on("/reset", handleReset);
  server.begin();
  Serial.println("HTTP server started");
  
}
void setup() {
  Serial.begin(115200);
  delay(10);
    
  if (digitalRead(0) == LOW) {
    handleReset();
  }
  // check if it has connection settings in flash - looking for the magic sequence ("jadelabs" at first 8 bytes of flash)
   
  char* signature=readFlash(0,8);
  Serial.printf("Signature read:%s\n",signature);
  
  if (strstr(signature,"jadelabs")){
    // signature found, loading ssid, password and ip
    ssid=readFlash(10,SSID_SIZE);
    password= readFlash(10+SSID_SIZE,PWD_SIZE);
    host = readFlashIP(10+SSID_SIZE+PWD_SIZE);
    APMode = false;

    Serial.println();
    Serial.println();
    Serial.printf("Connecting to %s with %s\n",ssid, password);
    Serial.printf("ServerIp is %s\n",host);
    
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    
    int counter = 0;
    while (WiFi.status() != WL_CONNECTED and not(APMode)) {
      delay(100);
      Serial.print(".");
      counter+=1;
      if( counter == 100)
        APMode = true;
        
    }
    if (APMode) {
      ssid     = "jadelabs";
      password = "jadelabs";
      wifiSetup();
    }
    else{
      Serial.println("");
      Serial.println("WiFi connected");  
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());
    }

  }
  else{
    APMode = true;
    Serial.println("Signature not found... entering AP Mode");
    // signature not found, switching to AP mode
    wifiSetup();
  }

}

void handleSet(){
  String pass = server.arg("pass");
  String ssid = server.arg("ssid");
  String ip = server.arg("ip");
  String response ="";
  if (pass != ""){
    writeFlash(pass,10+SSID_SIZE,PWD_SIZE);
    response+= "<h1>Password is "+pass+"</h1>";
  }
  if (ssid != ""){
    writeFlash(ssid,10,SSID_SIZE);
    writeFlash("jadelabs",0,10);
    response+= "<h1>SSID is "+ssid+"</h1>";
  }
  if (ip != ""){
    writeFlash(ip,30,10);
    response+= "<h1>IP is "+ip+"</h1>";
  }
  server.send(200, "text/html", response);
  setup();
  
}


void handleReset(){
  String response ="";
  clearFlash();
  response+= "<h1>Factory settings restored</h1>";
  server.send(200, "text/html", response);
  setup();
  
}


int value = 0;
boolean pressed = false;

void loop() {

  if (not(APMode)){
    //delay(5000);
    ++value;
    //client mode
    Serial.print("connecting to ");
    Serial.println(host);
    
    // Use WiFiClient class to create TCP connections
    WiFiClient client;
    const int httpPort = 80;
    if (!client.connect(host, httpPort)) {
      Serial.println("connection failed");
      if (value >=4) {
        ssid     = "jadelabs";
        password = "jadelabs";
        wifiSetup();
        APMode=true;
      }
      return;
    }
    if (digitalRead(0) == LOW) {
      pressed =true;
    }
    else {
      if (pressed)  {
        pressed = false;
        // We now create a URI for the request
        String url = "/open";
        
        Serial.print("Requesting URL: ");
        Serial.println(url);
        
        // This will send the request to the server
        client.print(String("GET ") + url + " HTTP/1.1\r\n" +
                     "Host: " + host + "\r\n" + 
                     "Connection: close\r\n\r\n");
        unsigned long timeout = millis();
        while (client.available() == 0) {
          if (millis() - timeout > 5000) {
            Serial.println(">>> Client Timeout !");
            if (value >=4) {
              ssid     = "jadelabs";
              password = "jadelabs";
              wifiSetup();
              APMode=true;
            }
            client.stop();
            return;
          }
        }
        
        // Read all the lines of the reply from server and print them to Serial
        while(client.available()){
          String line = client.readStringUntil('\r');
          Serial.print(line);
        }
        
        Serial.println();
        Serial.println("closing connection");
      }
    }
  }
  else{
    value = 0;
    //ap mode
    server.handleClient();
  }
}

