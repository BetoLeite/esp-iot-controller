/*
 * Copyright (c) 2015, Majenko Technologies
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 * 
 * * Neither the name of Majenko Technologies nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Create a WiFi access point and provide a web server on it. */

#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <EEPROM.h>

#define MEM_ALOC_SIZE 20
uint8_t passwd = 0;

/* Set these to your desired credentials. */
char gssid[10] = "DoorCtrl";
char gpassword[10] = "12345678";

ESP8266WebServer server(80);

void flashSetup(){
  Serial.begin(115200);
  Serial.printf("Flash chip ID: %d\n", ESP.getFlashChipId());
  Serial.printf("Flash chip size (in bytes): %d\n", ESP.getFlashChipSize());
  Serial.printf("Flash chip speed (in Hz): %d\n", ESP.getFlashChipSpeed());
  EEPROM.begin(MEM_ALOC_SIZE);
  unsigned char passwd[10]={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}; 
  unsigned char ssid[10]={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

  Serial.printf("\nFlash content:\n");
  for(int i=0;i<9;i++){
    unsigned char pass_char = EEPROM.read(i);
    unsigned char ssid_char = EEPROM.read(10+i);
    //if (pass_char!=0) passwd[i] = pass_char;
    //if (ssid_char!=0) ssid[i] = ssid_char;
    passwd[i] = pass_char;
    ssid[i] = ssid_char;
  }
  Serial.print(">");
  for(int i=0;i<10;i++){
    if (ssid[i] ==0) break;
    Serial.printf("%c",ssid[i]);
  }
  Serial.println("<");
  Serial.print(">");
  for(int i=0;i<10;i++){
    if (passwd[i] ==0) break;
    Serial.printf("%c",passwd[i]);
  }
  Serial.println("<");
  
  EEPROM.end();
  Serial.printf("\n");
  memcpy ( gssid, ssid, 10 );
  memcpy ( gpassword, passwd, 10 );
  
}

void emptyFlash(int offset){
  EEPROM.begin(MEM_ALOC_SIZE);
  Serial.println("\nErasing flash:");
  for(int i=0;i<10;i++){
    Serial.println(i+offset);
    EEPROM.write(i+offset,0);
  }
  Serial.println("Flash erased");
}
void setcontent(String content, int offset){
  EEPROM.begin(MEM_ALOC_SIZE);
  unsigned char mydata[8]; 
/*  Serial.printf("\nflash content:");
  for(int i=0;i<10;i++){
    mydata[i] = EEPROM.read(i+offset);
    Serial.printf("%c",mydata[i]);
    if (i==10) Serial.println();
  }
*/
  unsigned char *p=new unsigned char[content.length()+1];
  strcpy((char *)p,content.c_str());  
  emptyFlash(offset);
  Serial.printf("\nwriting flash...\n");
  for (int i=0; i<= 10;i++){
    if (i< content.length()) {
      Serial.printf("writing position %i:%c %i\n",i+offset,p[i],p[i]);
      EEPROM.write(i+offset,p[i]);
    }
    else{
      Serial.printf("writing position %i:%c %i\n",i+offset,p[i],0);
      EEPROM.write(i+offset,0);
    }
  }
  Serial.printf("\nre-reading configuration...\n>");
  p[0] = EEPROM.read(0);
  
  for(int i=0;i<10;i++){
    mydata[i] = EEPROM.read(i+offset);
    if (mydata[i] != 0){
      Serial.printf("%c",mydata[i]);
    }
    if (i==10) Serial.println();
  }
  Serial.println("<");
  EEPROM.end();  
}

void handleReset(){
  String response ="";
  String pass = "12345678";
  String ssid = "DoorCtrl";
  String empty ;
  //erase all flash
  emptyFlash(0); 
  emptyFlash(10); 
  setcontent(pass,0);
  response+= "<h1>Password is "+pass+"</h1>";
  setcontent(ssid,10);
  response+= "<h1>SSID is "+ssid+"</h1>";
  server.send(200, "text/html", response);
  flashSetup();
  wifiSetup();
}

void handleSet(){
  String pass = server.arg("pass");
  String ssid = server.arg("ssid");
  String reboot = server.arg("reboot");
  String response ="";
  if (pass != ""){
    setcontent(pass,0);
    response+= "<h1>Password is "+pass+"</h1>";
  }
  if (ssid != ""){
    setcontent(ssid,10);
    response+= "<h1>SSID is "+ssid+"</h1>";
  }
  if (reboot != ""){
    response+="<h1>rebooting wifi</h1>";
  }
  server.send(200, "text/html", response);
  if (reboot != ""){
    flashSetup();
    wifiSetup();
  }
}
void handleRoot() {
  String pass="";
  pass = server.arg("pass");
  String response ;
  if (pass != ""){
    setcontent(pass,0);
    response = "<h1>Password is "+pass+"</h1>";
  }
  response = "<h1>You are connected. Door is locked </h1>";
  server.send(200, "text/html", response);
  digitalWrite(2, LOW);   
}

void handleOpen() {
  server.send(200, "text/html", "<h1>Door Opened</h1>");
  digitalWrite(2, HIGH);   
  delay(4000);
  handleClose();
  
}
void handleClose() {
  server.send(200, "text/html", "<h1>Door Locked</h1>");
  digitalWrite(2, LOW);   
}

void wifiSetup(){
  Serial.print("Configuring access point...\n");
  /* You can remove the password parameter if you want the AP to be open. */
  Serial.printf(">%s<\n",gssid);
  Serial.printf(">%s<\n",gpassword);
  WiFi.softAP(gssid, gpassword);
  //WiFi.softAP("DoorCtrl", "12345678");
  
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  
}
void setup() {
  delay(1000);
  
  if (digitalRead(0) == LOW) {
    handleReset();
  }
	pinMode(2, OUTPUT);
	Serial.println();
	for (int i=0; i <= 2; i++){
      digitalWrite(2, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(500);
      digitalWrite(2, LOW);   // turn the LED on (HIGH is the voltage level)
      delay(200);
   } 
  flashSetup();

  wifiSetup();
  server.on("/", handleRoot);
  server.on("/set", handleSet);
  server.on("/reset", handleReset);
  server.on("/open", handleOpen);
  server.on("/close", handleClose);

  server.begin();
	Serial.println("HTTP server started");
}

void loop() {
	server.handleClient();
}
